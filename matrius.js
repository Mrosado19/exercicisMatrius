let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}

// PINTAR TODO

function paintAll() {
    erase();

    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0;j < rows[i].children.length;j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

// BORRAR

function erase() {
    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0;j< rows[i].children.length;j++ ) { // afegir codi
            
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

// PINTAR LA MITAD DE LA DERECHA

function paintRightHalf() {
    erase();

    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=parseInt((rows[i].children.length)/2); j < rows[i].children.length; j++ ) { // afegir codi

            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

// PINTAR LA MITAD DE LA IZQUIERDA

function paintLeftHalf() {
    erase();

    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0; j < (Math.ceil((rows[i].children.length)/2)); j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

// PINTAR LA MITAD DE ARRIBA
function paintUpperHalf() {
    erase();

    for (let i=0;i < rows.length/2;i++ ) { // afegir codi
        for (let j=0;j < rows[i].children.length;j++ ) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();

    for (let i=0, r=rows[0].children.length;i < rows.length;i++, r--) { // afegir codi
        for (let j=0;j < rows[i].children.length-r;j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperTriangle() {
    erase();
    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0;j < rows[i].children.length - i;j++ ) { // afegir codi
            // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

// PINTAR EL BORDE

function paintPerimeter() {
    erase();
    paintAll();
    for (let i=1;i < (rows.length)-1;i++ ) {
        for (let j=1;j< (rows[i].children.length)-1;j++ ) {
            // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

// PINTAR COMO AJEDREZ

function paintCheckerboard() {
    erase();
    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0;j < rows[i].children.length;j++ ) { // afegir codi
            // matrix[1][2].style.backgroundColor = "red";
            if ((i%2==0)) {
                if((j%2)==0){
                    matrix[i][j].style.backgroundColor = "red";
                } else{
                    matrix[i][j].style.backgroundColor = "white";
                }
            }else{
                if((j%2)==1){
                    matrix[i][j].style.backgroundColor = "red";
                } else{
                    matrix[i][j].style.backgroundColor = "white";
                }
            }
        }
    }
}

// PINTAR COMO AJEDREZ CONTRARIO

function paintCheckerboard2() {
    erase();
    for (let i=0;i < rows.length;i++ ) { // afegir codi
        for (let j=0;j < rows[i].children.length;j++ ) { // afegir codi
            // matrix[1][2].style.backgroundColor = "red";
            if ((i%2==1)) {
                if((j%2)==0){
                    matrix[i][j].style.backgroundColor = "red";
                } else{
                    matrix[i][j].style.backgroundColor = "white";
                }
            }else{
                if((j%2)==1){
                    matrix[i][j].style.backgroundColor = "red";
                } else{
                    matrix[i][j].style.backgroundColor = "white";
                }
            }
        }
    }
}
